## For Bug Reports:

<!-- First, make sure your bug is new and hasn't been previously reported or already fixed -->

<!-- Be clear and concise with your issue, including in the title -->

### Expected behavior

<!-- What should've happened? -->

### Actual behavior

<!-- What happened? -->

### Steps to reproduce the problem

<!-- How did this bug happen? -->

### Other notes

<!-- Anything else we need to know about this case? -->

## For Feature/Interface Modification Requests:

<!-- Use a clear, concise, and distinct title for your request -->

### Pitch

<!-- Describe your idea for a feature or change to the interface. Make sure it hasn't been suggested or implemented before -->

### Motivation

<!-- Why do you think this feature or change to the interface is needed? And who would it benefit the most? -->