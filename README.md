# Traboone Social UI

Traboone Social UI is a set of customizations that is used to turn [Soapbox](https://soapbox.pub)/[Pleroma](https://pleroma.social) into Traboone Social.

## Copyright

(C) 2020 Sandia Mesa Animation Studios and other contributors (see [AUTHORS.md](AUTHORS.md))<br>
(C) 2016-2020 Alex Gleason, Eugen Rochko, Gab AI, Inc, and other Soapbox, Mastodon, and Gab Social contributors

Except where otherwise stated, Traboone Social UI is distributed under the GNU Affero General Public License version 3. See [COPYING](COPYING) for more details.

Traboone Social UI is free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Traboone Social UI is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://gnu.org/licenses/.
