# Authors
Traboone UI is available on Sandia Mesa's self-hosted [Gitea](https://code.sandiamesa.com/sandiamesa/traboone-ui) and is provided thanks to the work of the following contributors:

* Sean King - <sean.king@sandiamesa.com>

Additionally, Sandia Mesa is grateful for the work done by [Soapbox](https://gitlab.com/soapbox-pub/soapbox), [Gab Social](https://code.gab.com/gab/social/gab-social/-/blob/develop/AUTHORS.md), and [Mastodon](https://github.com/tootsuite/mastodon/blob/master/AUTHORS.md) contributors to help the social media side of the fediverse become what it is today. They've also been an inspiration for us to continue to embrace, use, and publish free and open-source software as part of our business model.