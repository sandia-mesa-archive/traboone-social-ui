# Contributing
Thank you for considering contributing to the Traboone UI project. Here are some ways you can contribute:

You can contribute in the following ways:

* Finding and reporting bugs
* Contributing code to Traboone UI by fixing bugs or implementing new features
* Improving the documentation

## Bug Reports and Feature Suggestions
Bug reports and feature suggestions can be submitted to the [Gitea Issues](https://code.sandiamesa.com/sandiamesa/traboone-ui/issues) tracker for Traboone UI. Please make sure that you're not submitting duplicates, and that a similar report or request has not already been resolved or rejected in the past using the search function. Also, please use descriptive and concise with your titles.