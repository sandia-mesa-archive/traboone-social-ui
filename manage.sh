#!/usr/bin/env bash
# This is a bash script for managing Traboone Social UI.
# For help with running it, see ./manage.sh --help.

# Functions
get-moderators-covenant() {
  mkdir .tmp
  git clone https://code.sandiamesa.com/traboone/moderators-covenant --depth 1 .tmp/moderators-covenant
  rm -f static/instance/about/moderators-covenant.html
  cp .tmp/moderators-covenant/other-formats/covenant.html static/instance/about/moderators-covenant.html
  rm -rf .tmp
}

get-emoji-packs() {
  mkdir .tmp
  git clone https://code.sandiamesa.com/traboone/tramoji --depth 1 .tmp/tramoji
  rm -rf static/emoji/tramoji
  mkdir static/emoji/tramoji
  cp .tmp/tramoji/{pack.json,*.png} static/emoji/tramoji
  rm -rf .tmp
}

get-mastofe-build() {
  mkdir .tmp
  version=$(git ls-remote --tags https://git.pleroma.social/traboone/mastofe.git | grep -o "refs/tags/v[0-9]*\.[0-9]*" | sort -rV | head -1 | grep -o '[^\/]*$')
  curl -L https://git.pleroma.social/traboone/mastofe/-/jobs/artifacts/${version}/download?job=build-release -o .tmp/mastofe.zip --progress-bar
  if [ -d "static/packs" ] && [ -f "static/sw.js" ]; then
    rm -rf static/packs
    rm -f static/sw.js
  fi
  mkdir .tmp/mastofe
  busybox unzip .tmp/mastofe.zip -o -d .tmp/mastofe > /dev/null
  cp -r .tmp/mastofe/public/packs static
  cp .tmp/mastofe/public/assets/sw.js static
  rm -rf .tmp
}

help() {
  echo -e "Usage: $(basename "$0") <task>

  The only task that can currently be executed by this script is:

  get-moderators-covenant
    Get the HTML-formatted moderators' covenant from Sandia Mesa
    Code and places it in the static/instance/about folder.

  get-emoji-packs
    Get emoji packs, such as Tramoji, and copy their pack.json
    and .png files to the proper directory.

  get-mastofe-build
    Get latest build of custom Traboone Social MastoFE from
    Pleroma's GitLab.
  "
}

# Main program
if [ -z "$1" ] || [ "$1" = "--help" ]; then
  help
else
  task="$1"
  if [[ $task = "get-moderators-covenant" || $task = "get-emoji-packs" || $task = "get-mastofe-build" ]]; then
    $task
  else
    help
  fi
fi
